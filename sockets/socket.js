const {io} = require('../index');

//Send message sockets
io.on('connection', client => {
    // client.on('event', data => { /* … */ });
    console.log("Cliente conectado")
    client.on('disconnect', () => {
        console.log("Cliente desconectado")
    });

    client.on('message', (payload)=>{
        console.log(payload);
        io.emit('message', {admin: 'Nuevo mensaje con docker2'});
    })
});