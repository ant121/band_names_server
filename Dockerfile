FROM node:16-alpine3.14

WORKDIR /app
COPY . .

CMD ["node", "/app/index.js"]